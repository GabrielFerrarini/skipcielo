FROM node:alpine

EXPOSE 4000

WORKDIR /usr/src/app

RUN apk update && apk upgrade && apk add git

COPY . .

RUN yarn global add npm && npm install

CMD ["npm", "run", "watch"]