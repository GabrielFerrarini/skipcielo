const express = require('express');
const cors = require('cors');

const CardRouter = require('./routes/card');

const app = express();

app.use(cors({ origin: '* ' }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(CardRouter);

app.listen(3000, () => {
  console.log('App listening');
});

module.exports = app;
