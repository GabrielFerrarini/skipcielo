const httpStatus = require('http-status-codes');
const superagent = require('superagent');
const config = require('config');

const apiUrl = config.get('apiUrl');
const apiQueryUrl = config.get('apiQueryUrl');
const merchantId = config.get('merchantId');
const merchantKey = config.get('merchantKey');

const CardController = {
  // eslint-disable-next-line
  async tokenize(request, response, next) {
    try {
      const card = await superagent
        .post(`${apiUrl}/1/card`)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('MerchantId', merchantId)
        .set('MerchantKey', merchantKey)
        .send(request.body);

      const result = JSON.parse(card.text);
      console.log(JSON.stringify(result));

      return response.status(httpStatus.CREATED).json(result);
    } catch (error) {
      return next({
        error: httpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal error',
      });
    }
  },

  // eslint-disable-next-line
  async readCreditCard(request, response, next) {
    try {
      const { cctoken } = request.params;

      const card = await superagent
        .get(`${apiQueryUrl}/1/card/${cctoken}`)
        .set('Content-Type', 'application/json')
        .set('MerchantId', merchantId)
        .set('MerchantKey', merchantKey);

      const result = JSON.parse(card.text);
      console.log(JSON.stringify(result));

      return response.status(httpStatus.OK).json(result);
    } catch (error) {
      return next({
        error: httpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal error',
      });
    }
  },
};

module.exports = CardController;
