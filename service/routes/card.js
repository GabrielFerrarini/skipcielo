const express = require('express');

const CardController = require('../controllers/cardController');

const CardRouter = express.Router();

CardRouter.route('/card')
  .post(CardController.tokenize);

CardRouter.route('/card/:cctoken')
  .get(CardController.readCreditCard);

module.exports = CardRouter;
