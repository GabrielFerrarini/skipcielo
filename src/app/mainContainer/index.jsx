import React from 'react';
import PropTypes from 'prop-types';

const MainContainer = props => (
  <main>
    {props.children}
  </main>
);

MainContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainContainer;
