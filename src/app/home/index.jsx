import React from 'react';

const Home = () => (
  <div id="home-container" className="base-container">
    <h1>Home</h1>
  </div>
);

export default Home;
