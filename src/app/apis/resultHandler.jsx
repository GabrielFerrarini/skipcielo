export default function (response) {
  if (!response.ok) {
    throw new Error(response.status);
  }

  return response;
}
