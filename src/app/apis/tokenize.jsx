import { contentJsonHeader } from './headers';
import handler from './resultHandler';

function tokenize({
  CustomerName,
  CardNumber,
  Holder,
  ExpirationDate,
  Brand,
}) {
  const headers = contentJsonHeader();

  return fetch(`${CIELO_API_URL}/card`, {
    method: 'POST',
    headers,
    body: JSON.stringify({
      CustomerName,
      CardNumber,
      Holder,
      ExpirationDate,
      Brand,
    }),
  })
    .then(handler)
    .then(res => res.json())
    .then(data => data);
}

export default {
  tokenize,
};
