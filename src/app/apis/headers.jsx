export const contentJsonHeader = (header = null) => {
  const newHeader = header || new Headers();

  newHeader.append('Content-Type', 'application/json');

  return newHeader;
};

export const acceptJsonHeader = (header = null) => {
  const newHeader = header || new Headers();

  newHeader.append('Accept', 'application/json');

  return newHeader;
};
