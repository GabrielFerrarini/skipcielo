import { acceptJsonHeader, contentJsonHeader } from './headers';
import handler from './resultHandler';

function getCard({ CardToken }) {
  const headers = contentJsonHeader(acceptJsonHeader());

  return fetch(`${CIELO_API_URL}/card/${CardToken}`, {
    method: 'GET',
    headers,
  })
    .then(handler)
    .then(res => res.json())
    .then(data => data);
}

export default {
  getCard,
};
