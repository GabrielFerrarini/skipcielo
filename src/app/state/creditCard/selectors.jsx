import { createSelector } from 'reselect';

const getTokenizeCreditCardFormSelector = state => state.form.login;
const getCreditCardSelector = state => state.creditCard;

export const tokenizeCreditCardFormSelector =
  createSelector([getTokenizeCreditCardFormSelector], (tokenizeCreditCardForm) => {
    if (tokenizeCreditCardForm && tokenizeCreditCardForm.values) {
      return {
        ...tokenizeCreditCardForm.values,
      };
    }

    return null;
  });

export const creditCardSelector =
  createSelector([getCreditCardSelector], creditCard => [...creditCard]);
