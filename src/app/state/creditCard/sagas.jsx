
import { all, takeLatest, call, put } from 'redux-saga/effects';

import tokenizeApi from '../../apis/tokenize';
import * as Constants from './constants';
import * as Actions from './actions';

function* tokenize(action) {
  try {
    const {
      CustomerName,
      CardNumber,
      Holder,
      ExpirationDate,
      Brand,
    } = action;
    const creditCard = yield call(tokenizeApi.tokenize, {
      CustomerName,
      CardNumber,
      Holder,
      ExpirationDate,
      Brand,
    });

    yield put(Actions.tokenizeCreditCardSuccess(creditCard));
  } catch (error) {
    yield put(Actions.tokenizeCreditCardError(error));
  }
}

function* watchTokenize() {
  yield takeLatest(Constants.TOKENIZE_REQUESTED, tokenize);
}

export default function* watchTokenizeSaga() {
  yield all([
    watchTokenize(),
  ]);
}
