import * as Constants from './constants';

const tokenizeInitialState = {
  requesting: false,
  error: null,
};

export const tokenizeReducer = function tokenizeReducer(state = tokenizeInitialState, action) {
  switch (action.type) {
    case Constants.TOKENIZE_REQUESTED:
      return {
        ...state,
        requesting: true,
      };
    case Constants.TOKENIZE_SUCCESS:
      return {
        ...state,
        requesting: false,
      };
    case Constants.TOKENIZE_ERROR:
      return {
        ...state,
        requesting: false,
        error: action.error,
      };
    default:
      return state;
  }
};

const creditCardInitialState = {
  requesting: false,
  creditCard: null,
  error: null,
};

export const creditCardReducer =
  function creditCardReducer(state = creditCardInitialState, action) {
    switch (action.type) {
      case Constants.FETCH_CREDIT_CARD_REQUESTED:
        return {
          ...state,
          requesting: true,
        };
      case Constants.FETCH_CREDIT_CARD_SUCCESS:
        return {
          ...state,
          requesting: false,
          creditCard: action.creditCard,
        };
      case Constants.FETCH_CREDIT_CARD_ERROR:
        return {
          ...state,
          requesting: false,
          error: action.error,
        };
      default:
        return state;
    }
  };
