import * as Constants from './constants';

export function tokenizeCreditCardRequested({
  CustomerName,
  CardNumber,
  Holder,
  ExpirationDate,
  Brand,
}) {
  return {
    type: Constants.TOKENIZE_REQUESTED,
    CustomerName,
    CardNumber,
    Holder,
    ExpirationDate,
    Brand,
  };
}

export function tokenizeCreditCardSuccess() {
  return {
    type: Constants.TOKENIZE_SUCCESS,
  };
}

export function tokenizeCreditCardError({ error }) {
  return {
    type: Constants.TOKENIZE_ERROR,
    error,
  };
}

export function fetchCreditCardRequested({ creditCardToken }) {
  return {
    type: Constants.FETCH_CREDIT_CARD_REQUESTED,
    creditCardToken,
  };
}

export function fetchCreditCardSuccess(creditCard) {
  return {
    type: Constants.FETCH_CREDIT_CARD_REQUESTED,
    creditCard,
  };
}

export function fetchCreditCardError(error) {
  return {
    type: Constants.FETCH_CREDIT_CARD_REQUESTED,
    error,
  };
}
