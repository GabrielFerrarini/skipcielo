import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { tokenizeReducer, creditCardReducer } from './creditCard/reducer';

export default combineReducers({
  form: formReducer,
  tokenize: tokenizeReducer,
  creditCard: creditCardReducer,
});
