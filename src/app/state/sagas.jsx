import { all } from 'redux-saga/effects';

import watchTokenizeSaga from './creditCard/sagas';

export default function* saga() {
  yield all([
    watchTokenizeSaga(),
  ]);
}
