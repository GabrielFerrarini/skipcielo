import React from 'react';
import {
  Router,
  Route,
  Switch,
} from 'react-router-dom';

import Home from './home';
import TokenizeCreditCardPage from './pages/tokenizeCreditCard';
import CreditCardListPage from './pages/creditCards';
import history from './history';

export default (
  <Router history={history}>
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/TokenizeCreditCard" component={TokenizeCreditCardPage} exact />
      <Route path="/CreditCards" component={CreditCardListPage} exact />
    </Switch>
  </Router>
);
