import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import TokenizeCreditCardForm from './components';

import * as Actions from '../../state/creditCard/actions';
import * as Selectors from '../../state/creditCard/selectors';

class TokenizeCreditCardPage extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.tokenize = this.tokenize.bind(this);
  }

  tokenize() {
    const {
      CustomerName,
      CardNumber,
      Holder,
      ExpirationDate,
      Brand,
    } = this.props.tokenizeCreditCardForm;

    this.props.tokenizeCreditCard({
      CustomerName,
      CardNumber,
      Holder,
      ExpirationDate,
      Brand,
    });
  }

  render() {
    return (
      <TokenizeCreditCardForm onSubmit={this.tokenize} />
    );
  }
}

TokenizeCreditCardPage.defaultProps = {
  tokenizeCreditCardForm: {
    CustomerName: null,
    CardNumber: null,
    Holder: null,
    ExpirationDate: null,
    Brand: null,
  },
};

TokenizeCreditCardPage.propTypes = {
  tokenizeCreditCardForm: PropTypes.shape({
    CustomerName: PropTypes.string,
    CardNumber: PropTypes.string,
    Holder: PropTypes.string,
    ExpirationDate: PropTypes.string,
    Brand: PropTypes.string,
  }),
  tokenizeCreditCard: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  tokenizeCreditCardForm: Selectors.tokenizeCreditCardFormSelector(state),
});

const mapDispatchToProps = {
  tokenizeCreditCard: Actions.tokenizeCreditCardRequested,
};

export default connect(mapStateToProps, mapDispatchToProps)(TokenizeCreditCardPage);

