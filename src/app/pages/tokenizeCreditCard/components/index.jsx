import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Field, reduxForm } from 'redux-form';

class TokenizeCreditCardForm extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    const renderTextField = ({
      input,
      label,
      ...custom
    }) =>
      (
        <TextField
          placeholder={label}
          {...input}
          {...custom}
        />
      );

    const {
      handleSubmit,
      pristine,
      submitting,
      reset,
    } = this.props;

    return (
      <form onSubmit={handleSubmit} >
        <div id="credit-card-tokenize-form" className="form-container">
          <div id="credit-card-tokenize-form-name">
            <Field
              name="CustomerName"
              component={renderTextField}
              label="Customer Name"
            />
          </div>
          <div id="credit-card-tokenize-form-card">
            <Field
              name="CardNumber"
              component={renderTextField}
              label="Card Number"
            />
          </div>
          <div id="credit-card-tokenize-form-holder">
            <Field
              name="Holder"
              component={renderTextField}
              label="Holder"
            />
          </div>
          <div id="credit-card-tokenize-form-expiration">
            <Field
              name="ExpirationDate"
              component={renderTextField}
              label="Expiration Date"
            />
          </div>
          <div id="credit-card-tokenize-form-branc">
            <Field
              name="Brand"
              component={renderTextField}
              label="Brand"
            />
          </div>
          <Button type="submit" color="primary" disabled={pristine || submitting}>
            Submit
          </Button>
          <Button color="secondary" disabled={pristine || submitting} onClick={reset}>
            Clear Values
          </Button>
        </div>
      </form>
    );
  }
}

TokenizeCreditCardForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

const ConnectedLoginForm = connect()(TokenizeCreditCardForm);

export default reduxForm({
  form: 'login',
})(ConnectedLoginForm);
