import React from 'react';
import PropTypes from 'prop-types';

const CreditCardList = props => (
  <div id="credit-card-tokenize-form" className="form-container">
    <ul>
      {props.creditCards.forEach(creditCard => (<li>{creditCard.name}</li>))}
    </ul>
  </div>
);

CreditCardList.defaultProps = {
  creditCards: [],
};

CreditCardList.propTypes = {
  creditCards: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
  })),
};

export default CreditCardList;
