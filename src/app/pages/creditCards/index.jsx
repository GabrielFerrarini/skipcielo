import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import CreditCardsList from './components/creditCardList';

import * as Actions from '../../state/creditCard/actions';
import * as Selectors from '../../state/creditCard/selectors';

class CreditCardsPage extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {
    this.props.fetchCreditCards('0aa9769e-b301-4ae5-a35b-72fa0920679c');
  }

  render() {
    return (
      <CreditCardsList creditCards={this.props.creditCards} />
    );
  }
}

CreditCardsPage.defaultProps = {
  creditCards: [],
};

CreditCardsPage.propTypes = {
  creditCards: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
  })),
  fetchCreditCards: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  creditCards: Selectors.creditCardSelector(state),
});

const mapDispatchToProps = {
  fetchCreditCards: Actions.fetchCreditCardRequested,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreditCardsPage);

