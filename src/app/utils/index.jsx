// eslint-disable-next-line import/prefer-default-export
export const curry = (fn) => {
  if (fn.length === 0) {
    return fn;
  }

  const nest = (N, args) => (
    (x) => {
      if (N - 1 === 0) {
        return fn(...args, x);
      }
      return nest(N - 1, [...args, x]);
    });

  return nest(fn.length, []);
};
