# How to run this project

#### Go to root directory
#### docker-compose up --build -d

https://bitbucket.org/GabrielFerrarini/skipcielo/

This project is an interface to add tokenized credit cards to Cielo service

Follow the instructions and go to http://localhost:4000/TokenizeCreditCard to tokenize a credit card.



Sample data:



{

    "CustomerName": "Paulo Henrique",

    "CardNumber":"4929592299120131",

    "Holder":"Teste Holder",

    "ExpirationDate":"12/2018",

    "Brand":"Visa"

}